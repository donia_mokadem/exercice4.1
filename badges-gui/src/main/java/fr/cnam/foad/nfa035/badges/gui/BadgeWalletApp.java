package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.gui.view.AddBadgeDialog;
import fr.cnam.foad.nfa035.badges.gui.view.BadgeWalletGUI;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.swing.*;
import java.awt.*;

@Configuration
@SpringBootApplication
public class BadgeWalletApp {

    /**
     * Commentez-moi
     * @param args les arguments
     */
    public static void main(String[] args) {

        ConfigurableApplicationContext ctx = new SpringApplicationBuilder(BadgeWalletApp.class)
                .headless(false).run(args);

        EventQueue.invokeLater(() -> {
            JFrame frame = new JFrame("My Badge Wallet");

            BadgeWalletGUI gui = ctx.getBean(BadgeWalletGUI.class);

            frame.setContentPane(gui.getPanelParent());
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setLocationRelativeTo(null);
            frame.setIconImage(gui.getIcon().getImage());
            frame.setVisible(true);
        });


    }
}
